﻿using UnityEngine;

public class Cheese : Ingredient {

	protected override void Start () {
		base.Start();
		ingType = "cheese";
		value = 8;
		id = 5;
	}
	
}
