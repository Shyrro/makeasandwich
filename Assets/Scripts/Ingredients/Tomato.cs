﻿using UnityEngine;

public class Tomato : Ingredient {

	protected override void Start () {
		base.Start();
		ingType = "tomato";
		value = 7;
		id = 2;
	}
}
