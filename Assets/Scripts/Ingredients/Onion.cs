﻿using UnityEngine;

public class Onion : Ingredient {

	// Use this for initialization
	protected override void Start () {
		base.Start();
		ingType = "onion";
		value = 2;
		id = 4;
	}
	
}
