﻿using UnityEngine;

public class Ingredient : MonoBehaviour
{

    [HideInInspector]
    public int id;
    [HideInInspector]
    public string ingType;
    [HideInInspector]
    public int value;

    float damp = 0.9f;
    public static float fallingSpeed = 180f;
    float deltaFall = 0f;

    Rigidbody rb;
    Collider ingredientCollider;
    GameObject ingredientsContainer;
    GameObject plate;

    string tagAfterCollision = "IngredientOnPlate";
    string ingredientsContainerName = "IngredientsContainer";
    string plateTag = "DetectionZone";
    string playerLayer = "Player";
    string ingredientLayer = "Ingredient";

    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        ingredientCollider = GetComponent<Collider>();
        ingredientsContainer = GameObject.Find(ingredientsContainerName);
        plate = GameObject.Find("plate");

        int playerLayerIndex = LayerMask.NameToLayer(playerLayer);
        int ingredientLayerIndex = LayerMask.NameToLayer(ingredientLayer);

        Physics.IgnoreLayerCollision(playerLayerIndex, ingredientLayerIndex);
    }

    void Update()
    {
        if (ingredientCollider.tag.Equals(tagAfterCollision))
        {
            transform.position = new Vector3(plate.transform.position.x, transform.position.y, transform.position.z);
        }
    }

    void FixedUpdate()
    {
        if (!ingredientCollider.tag.Equals(tagAfterCollision))
            deltaFall = -fallingSpeed * Time.fixedDeltaTime;
        if (rb != null)
            rb.velocity = new Vector3(0, deltaFall, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(plateTag))
        {
            AddIngredientOnPlate(other, false);
        }
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {

        if ((other.collider.tag.Equals(tagAfterCollision)))
        {
            Physics.IgnoreCollision(ingredientCollider, other.collider, true);
        }

        if (other.collider.tag.Equals("Ground"))
        {
            Destroy(gameObject);
        }
    }

    private void AddIngredientOnPlate(Collider collisionWith, bool collision)
    {

        rb.drag = 5f;
        ingredientCollider.tag = tagAfterCollision;
        //The main purpose of the damp is to push the object a little bit upward so that they don't bounce of each other
        deltaFall *= 1 - damp;

        gameObject.transform.parent = ingredientsContainer.transform;

        //This is not working as expected
        //Try raycasts                
        transform.position = new Vector3(plate.transform.position.x, transform.position.y, transform.position.z);

        FixedJoint fj = gameObject.AddComponent<FixedJoint>();
        fj.connectedBody = collisionWith.gameObject.GetComponent<Rigidbody>();

    }

}