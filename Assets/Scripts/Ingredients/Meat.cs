﻿using UnityEngine;

public class Meat : Ingredient {

	protected override void Start () {
		base.Start();
		ingType = "meat";
		value = 10;
		id = 1;
	}
}
