﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour {

	private float speed = 10f;    
	private float inputX = 0f;
	[HideInInspector]
	public static bool lookingLeft = true;    

	public Rigidbody rb;

	// Update is called once per frame
	void Update() {
        inputX = CrossPlatformInputManager.GetAxis("Horizontal");

        //if (inputX > 0 && lookingLeft)
        //{
        //    transform.Rotate(0, 180, 0);
        //    lookingLeft = false;
        //}
        //else if (inputX < 0 && !lookingLeft)
        //{
        //    transform.Rotate(0, -180, 0);
        //    lookingLeft = true;
        //}
    }

	void FixedUpdate() {

        Vector3 force = new Vector3(inputX * speed * Time.fixedDeltaTime, 0, 0);
        rb.AddForce(force, ForceMode.VelocityChange);
    }

}