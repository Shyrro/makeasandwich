﻿using UnityEngine;

public class BadTomato : Ingredient
{

    protected override void Start()
    {
        base.Start();
        ingType = "badTomato";
        value = 7;
        id = 2;
    }
}
