﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateWallCollision : MonoBehaviour {

	string wallLayer = "Wall";
	string plateLayer = "Plate";

	// Use this for initialization
	void Start () {
		int plateLayerIndex = LayerMask.NameToLayer(plateLayer);
		int wallLayerIndex = LayerMask.NameToLayer(wallLayer);
		Physics.IgnoreLayerCollision(plateLayerIndex, wallLayerIndex);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
