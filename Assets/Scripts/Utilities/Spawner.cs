﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public List<GameObject> objectsToSpawn;
    float maxLeftPos = -7f;
    float maxRightPos = 7f;
    bool isSpawning = true;
    [HideInInspector]
    public float timeBtwSpawn = 2f;
    private int _diffNbSpawn;
    // Number to substract depending on the level
    // The higher the number is, the lesser we spawn
    [HideInInspector]
    public int DiffNbSpawn
    {
        get
        {
            return _diffNbSpawn;
        }
        set
        {
            if(value > objectsToSpawn.Count)
            {
                _diffNbSpawn = objectsToSpawn.Count;
            }
            else if(value < 0)
            {
                _diffNbSpawn = 0;
            }else
            {
                _diffNbSpawn = value;
            }
        }
    }
    

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndSpawn());
    }

    IEnumerator WaitAndSpawn()
    {
        while (isSpawning)
        {
            yield return new WaitForSeconds(timeBtwSpawn);
            //add logic for penalties and obstacles to avoid
            SpawnIngredient();
        }

    }

    private void SpawnIngredient()
    {
        float randomX = Random.Range(maxLeftPos, maxRightPos);
        Vector3 spawnPosition = new Vector3(randomX, transform.position.y - 1, 0);
        int randomNumber = Random.Range(0, objectsToSpawn.Count - DiffNbSpawn);
        Instantiate(objectsToSpawn[randomNumber], spawnPosition, Quaternion.identity);
    }

}